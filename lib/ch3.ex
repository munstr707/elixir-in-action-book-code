defmodule CH3.Geometry.Rectangle do
  def area({a, b}) do
    a * b
  end
end
