defmodule Geometry.Rectangle do
  def area(a, b) do
    a * b
  end
end

defmodule TestModule do
  import IO
  alias Geometry.Rectangle, as: Rectangle

  def area(a, b) do
    Rectangle.area(a, b)
  end

  def rectangle_area_shorter(a, b), do: a * b

  def piping_example() do
    -5 |> abs() |> Integer.to_string() |> IO.puts()
  end

  def sum(a, b) do
    a + b
  end

  def sum(a) do
    sum(a, 0)
  end

  def sum_default(a, b \\ 0) do
    a + b
  end

  defp sum_private(a, b, c) do
    a + b + c
  end

  def print_content(a) do
    puts(a)
  end
end

defmodule Geometry.Circle do
  @moduledoc """
    Circle module
  """
  @pi_test 3.14159

  @doc "area function"
  def area(radius) do
    radius * radius * @pi_test
  end
end
