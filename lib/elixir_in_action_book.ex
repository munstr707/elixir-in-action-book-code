defmodule ElixirInActionBook do
  @moduledoc """
  Documentation for `ElixirInActionBook`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ElixirInActionBook.hello()
      :world

  """
  def hello do
    :world
  end
end
